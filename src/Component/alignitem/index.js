import React from 'react';
import {View, Button, Text} from 'react-native';
import {Styles} from './styles';

const alignItem = () => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.text}>
        Welcome to my Align Items:{'\n'}
        <Text style={Styles.topDetails}>
          alignItems describes how to align children along the cross axis of
          their container.
        </Text>
      </Text>
      <Text style={Styles.textDetails}>
        baseline: Align children of a container along a common baseline.
        Individual children can be set to be the reference baseline for their
        parents.
      </Text>

      <View
        style={{
          flex: 1,
          alignItems: 'baseline',
          margin: 25,
        }}>
        <View
          style={{
            height: 50,
            width: 50,
            backgroundColor: 'red',
          }}></View>
        <View
          style={{
            height: 50,
            width: 50,
            backgroundColor: 'green',
          }}></View>
        <View
          style={{
            width: 50,
            height: 50,
            backgroundColor: 'pink',
          }}></View>
      </View>
    </View>
  );
};
export default alignItem;
