import React from 'react';
import {View, Button, Text} from 'react-native';
import {Styles} from './styles';

const Justify = () => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.text}>
        Welcome to my JustifyContent:{'\n'}{' '}
        <Text style={Styles.topDetails}>
          how to align children within the main axis of their container.{' '}
        </Text>
      </Text>
      <Text style={Styles.textDetails}>
        space-evenly: Evenly distribute children within the alignment container
        along the main axis. The spacing between each pair of adjacent items,
        the main-start edge and the first item, and the main-end edge and the
        last item, are all exactly the same.
      </Text>

      <View
        style={{
          flex: 1,
          justifyContent: 'space-evenly',
          backgroundColor: '#5674ee',
        }}>
        <View
          style={{
            width: 80,
            height: 80,
            backgroundColor: 'red',
          }}></View>
        <View
          style={{
            width: 80,
            height: 80,
            backgroundColor: 'green',
          }}></View>
        <View
          style={{
            width: 80,
            height: 80,
            backgroundColor: 'pink',
          }}></View>
      </View>
    </View>
  );
};
export default Justify;
