import React from 'react';
import {View, Button, Text} from 'react-native';
import {Styles} from './styles';

const AlignSelf = () => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.text}>
        Welcome to my Align Content:{'\n'}
        <Text style={Styles.topDetails}>
          Align Content is more Individual than the whole items, inorder words
          it over rides the parents function
        </Text>
      </Text>
      <Text style={Styles.textDetails}>
        stretch: Align child that is selected will take full space and others
        remains thier positions. parents.
      </Text>

      <View
        style={{
          flex: 1,
          margin: 25,
        }}>
        <View
          style={{
            alignSelf: 'stretch',
            height: 50,
            backgroundColor: 'red',
          }}></View>
        <View
          style={{
            height: 50,
            width: 50,
            backgroundColor: 'green',
          }}></View>
        <View
          style={{
            width: 50,
            height: 50,
            backgroundColor: 'pink',
          }}></View>
      </View>
    </View>
  );
};
export default AlignSelf;
