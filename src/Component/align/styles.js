import {StyleSheet} from 'react-native';
export const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex:1,
    backgroundColor:'grey'
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 16,
    color:'white'
  },
  textdetails:{
    fontSize:14,
    textAlign: 'center',
    color:'white',
    margin:10,
    


  },
  buttonContainer: {
    // flexDirection:'row',
    flex:1,
    // margin:15,
  },
});
