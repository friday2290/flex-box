import React from 'react';
import {View, Button, Text} from 'react-native';
import {Styles} from './styles';

const Flex = () => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.text}>Welcome to my flex box</Text>
      <Text style={Styles.textdetails}>
        Note that flex will“fill” over the available space along your main axis.
        flex:1, is the red, flexe:2 is the green and the flex:3 is the pink.
      </Text>

      <View style={Styles.buttonContainer}>
        <View
          style={{
            flex: 1,

            backgroundColor: 'red',
          }}></View>
        <View
          style={{
            flex: 2,

            backgroundColor: 'green',
          }}></View>
        <View
          style={{
            flex: 3,

            backgroundColor: 'pink',
          }}></View>
      </View>
    </View>
  );
};
export default Flex;
