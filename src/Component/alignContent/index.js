import React from 'react';
import {View, Button, Text} from 'react-native';
import {Styles} from './styles';

const AlignContent = () => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.text}>
        Welcome to my Align Content:{'\n'}
        <Text style={Styles.topDetails}>
        This only has effect when items are wrapped to multiple lines using flexWrap.It is different from 
        alignSelf, 
        </Text>
      </Text>
      <Text style={Styles.textDetails}>
        stretch: Align child that is selected will take full space and others
        remains thier positions. parents.
      </Text>

      <View
        style={{
          flex: 1,
          margin: 25,
        }}>
        <View
          style={{
            alignSelf: 'stretch',
            height: 50,
            backgroundColor: 'red',
          }}></View>
        <View
          style={{
            height: 50,
            width: 50,
            backgroundColor: 'green',
          }}></View>
        <View
          style={{
            width: 50,
            height: 50,
            backgroundColor: 'pink',
          }}></View>
      </View>
    </View>
  );
};
export default AlignContent;
