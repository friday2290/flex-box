import {StyleSheet} from 'react-native';
export const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: 'grey',
    flexDirection: 'column',
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 16,
    color: 'white',
  },
  textDetails: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
  },
});
