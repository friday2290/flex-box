import React from 'react';
import {View, Button, Text} from 'react-native';
import {Styles} from './styles';

const FlexDirection = () => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.text}>Welcome to my flex Direction</Text>
      <Text style={Styles.textDetails}>
        FlexDirection:'column-reverse' {'\n'}(default value) Align children from
        top to bottom. If wrapping is enabled, then the next line will start to
        the right of the first item on the top of the container
      </Text>

      <View style={{flex: 1, flexDirection: 'column-reverse'}}>
        <View
          style={{
            width: 80,
            height: 80,
            backgroundColor: 'red',
          }}></View>
        <View
          style={{
            width: 80,
            height: 80,
            backgroundColor: 'green',
          }}></View>
        <View
          style={{
            width: 80,
            height: 80,
            backgroundColor: 'pink',
          }}></View>
      </View>
    </View>
  );
};
export default FlexDirection;
