Flex Box In React Native.

Outlines.

1. Meaning of flex box .
2. Importance of flex box.
3. Type of flex box.

   Definition: 
   A Flexible Box Module, usually referred to as flexbox, was designed as a one-dimensional layout model, and as a method that could offer space distribution between items in an interface and powerful alignment capabilities.
   
   Advantages:
   Creating repeatable elements is quick and easy
   The advantages in using Flex box is that it minimises the code needed to create an alternating pattern significantly, particularly through the use of the 'order' property. This allows developers to reorder the display of content on the screen using one or two lines of code.
   Flex box allows us to have more control over alignment and behavior of boxes/divs/page elements when changing screen sizes or device orientation.
   
   Types:
    The flex container properties are:
   i. flex-direction.
   ii. flex-wrap.
   iii. flex-flow.
   iv. justify-content.
   v. align-items.
   vi. align-content.
   Briefly I'll talk about how each of them works and how to use them.
   First your flex of 1 takes the whole body, out of the body, flex of 2 take another part of the main container 
   But the parents container must have a flex of 1.

A. The flex-direction property defines in which direction the container wants to stack the flex items. It has four elements. 
Row
 Row reverse
 column and 
column reverse .

B. The flex-wrap property specifies whether the flex items should wrap or not.
It has three elements inside it namely 
A. Wrap
B. Nowrap
C. Wraprevers 
The flex-flow property is a shorthand property for setting both the flex-direction and flex-wrap properties. This has one elements inside it. Namely.
A. Row-wrap
The justify-content property is used to align the flex items: it has five elements
A. Centre 
B. Flex-start
C. Flex-end 
D. Space-around 
E. Space-between 
The align-items property is used to align the flex items.
A. Baseline
B. Stretch
C. Flex-end
D. Flex-start 
E. Centre

//below are the images for the whole image.

![picture1](./src/Component/asset/flex.png)
![picture1](./src/Component/asset/flexend.png)
![picture1](./src/Component/asset/flexdirectionrow.png)
![picture1](./src/Component/asset/rowreverse.png)
![picture1](./src/Component/asset/flexdirectioncolumn.png)
![picture1](./src/Component/asset/columnreverse.png)
![picture1](./src/Component/asset/justifycontentflexstart.png)
![picture1](./src/Component/asset/flexdirectionflexend.png)
![picture1](./src/Component/asset/alignitemflexend.png)
![picture1](./src/Component/asset/justifycontentspacearound.png)
![picture1](./src/Component/asset/justifycontentspaceevenly.png)
![picture1](./src/Component/asset/alignitemstrech.png)
![picture1](./src/Component/asset/alignitemsflexstart.png)
![picture1](./src/Component/asset/alignitemscenter.png)
![picture1](./src/Component/asset/alignitemsbaseline.png)
![picture1](./src/Component/asset/alignselfflexstart.png)
![picture1](./src/Component/asset/alignselfflexend.png)
![picture1](./src/Component/asset/alignselfcenter.png)
![picture1](./src/Component/asset/alignselfstrech.png)