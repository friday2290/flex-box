/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import AlignSelf from './src/Component/alignSelf';
// import alignItem from './src/Component/alignitem';
// import Flex from './src/Component/align';
// import FlexDirection from './src/Component/flexdirection';
// import Justify from './src/Component/justifycontent';

AppRegistry.registerComponent(appName, () => AlignSelf);
